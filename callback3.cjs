const fs = require('fs');

function getAllCardsDataBasedOnListID(findId, cb){

    fs.readFile('../cards_1.json', 'utf-8', (error, data) => {
        if(error){
            return cb("The file data is not read: " , error);
        }

        // convert json data in object
        const cardsData = JSON.parse(data);
        // get the all keys of cardsData
        const cardsKey = Object.keys(cardsData);
        let isIdPresent = false;

        setTimeout( () => {
            cardsKey.forEach( (currentKey) => {
                
                // if the currentKey and findId is match
                if(currentKey === findId){
                    isIdPresent = true;
                    // call the callback function
                    cb(null, cardsData[currentKey]);
                }
            });

            if(isIdPresent === false){
                cb("cards id is not present", null);
            }
        
        }, 2 * 1000);
    });

}

// export the getAllCardsDataBasedOnListID function
module.exports = getAllCardsDataBasedOnListID;