const fs = require('fs');
const getDataUsingboardID = require('./callback1.cjs');
const getAllListDataBasesOnBoardID = require('./callback2.cjs');
const getAllCardsDataBasedOnListID = require('./callback3.cjs');

function problem5(cb){

    fs.readFile('../boards_1.json', 'utf-8', (readingError, data) => {
        if(readingError){
            return cb("The file data is not read: " , readingError);
        }

        // convert json data in object
        const boardData = JSON.parse(data);
        let isIdPresent = false;

        setTimeout( () => {
            boardData.forEach( (currentObj) => {
                // get the current name
                const currentName = currentObj.name;
        
                // if the current name is Thanos
                if(currentName === "Thanos"){
                    // get the id of Thanos
                    const idOfThanos = currentObj.id;
                    isIdPresent = true;

                    // call the getDataUsingboardID function to get board data of thanos
                    getDataUsingboardID(idOfThanos, (error, result )=> {
                        if(error){
                            return cb(error);
                        }
                    
                        cb(null, result);
                    });
                
                    // call the getAllListDataBasesOnBoardID function to get list data of thanos
                    getAllListDataBasesOnBoardID(idOfThanos, (error, result )=> {
                        if(error){
                            return cb(error);
                        }
                    
                        cb(null, result);

                        result.forEach( (currentObj) => {
                            // check the current list name is Mind or Space
                            if(currentObj.name === "Mind" || currentObj.name === "Space"){
                                // get the id
                                const idIs = currentObj.id;
                                getAllCardsDataBasedOnListID(idIs, (error2, result2 )=> {
                                    if(error2){
                                        return cb(error2);
                                    }
                                
                                    cb(null, result2);
                                });
                            }
                        });
                    });
                }
            });

            if(isIdPresent === false){
                cb("Board id is not present", null);
            }
        
        }, 2 * 1000);

    });

}

// export the problem5 function
module.exports = problem5;