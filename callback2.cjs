const fs = require('fs');

function getAllListDataBasesOnBoardID(findId, cb){

    fs.readFile('../lists_1.json', 'utf-8', (error, data) => {
        if(error){
            return cb("The file data is not read: " , error);
        }

        // convert json data in object
        const listsData = JSON.parse(data);
        // get the all keys of listsData
        const keysOfListData = Object.keys(listsData);
        let isIdPresent = false;

        setTimeout( () => {
            keysOfListData.forEach( (currentKey) => {

                // if the currentKey and findId is match
                if(currentKey === findId){
                    isIdPresent = true;
                    // call the callback function
                    cb(null, listsData[currentKey]);
                }
            });

            if(isIdPresent === false){
                cb("list id is not present", null);
            }
        
        }, 2 * 1000);
    });

}

// export the getAllListDataBasesOnBoardID function
module.exports = getAllListDataBasesOnBoardID;