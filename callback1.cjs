const fs = require('fs');

function getDataUsingboardID(findId, cb){

    fs.readFile('../boards_1.json', 'utf-8', (error, data) => {
        if(error){
            return cb("The file data is not read: " , error);
        }

        // convert json data in object
        const boardData = JSON.parse(data);
        let isIdPresent = false;

        setTimeout( () => {
            boardData.forEach( (currentObj) => {
                // get the current id
                const currentId = currentObj.id;
        
                // if the currentId and findId is match
                if(currentId === findId){
                    isIdPresent = true;
                    // call the callback function
                    cb(null, currentObj);        
                }
            });

            if(isIdPresent === false){
                cb("Board id is not present", null);
            }
        
        }, 2 * 1000);
    });

}

// export the getDataUsingboardID function
module.exports = getDataUsingboardID;