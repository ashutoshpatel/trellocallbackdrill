const problem5 = require('../callback5.cjs');

problem5( (error, result )=> {
    if(error){
        console.log(error);
        return;
    }

    console.log(result);
});