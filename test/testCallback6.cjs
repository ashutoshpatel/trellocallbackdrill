const problem6 = require('../callback6.cjs');

problem6( (error, result )=> {
    if(error){
        console.log(error);
        return;
    }

    console.log(result);
});